(function () {
    let search, results, allWords = [];

    const indexOnTiengTrungCheckbox = document.getElementById('indexOnTiengTrungCheckbox');
    const indexStrategySelect = document.getElementById('indexStrategySelect');
    const removeStopWordsCheckbox = document.getElementById('removeStopWordsCheckbox');
    const indexOnTvKhongDauCheckbox = document.getElementById('indexOnTvKhongDauCheckbox');
    const indexOnTvCoDauCheckbox = document.getElementById('indexOnTvCoDauCheckbox');
    // const useStemmingCheckbox = document.getElementById('useStemmingCheckbox');
    const sanitizerSelect = document.getElementById('sanitizerSelect');
    const tfIdfRankingCheckbox = document.getElementById('tfIdfRankingCheckbox');

    const rebuildAndRerunSearch = function () {
        rebuildSearchIndex();
        searchWords();
    };

    indexOnTiengTrungCheckbox.onchange = rebuildAndRerunSearch;
    indexStrategySelect.onchange = rebuildAndRerunSearch;
    removeStopWordsCheckbox.onchange = rebuildAndRerunSearch;
    indexOnTvKhongDauCheckbox.onchange = rebuildAndRerunSearch;
    indexOnTvCoDauCheckbox.onchange = rebuildAndRerunSearch;
    // useStemmingCheckbox.onchange = rebuildAndRerunSearch;
    sanitizerSelect.onchange = rebuildAndRerunSearch;
    tfIdfRankingCheckbox.onchange = rebuildAndRerunSearch;

    const rebuildSearchIndex = function () {
        search = new JsSearch.Search('uid');

        if (useStemmingCheckbox.checked) {
            search.tokenizer = new JsSearch.StemmingTokenizer(stemmer, search.tokenizer);
        }

        if (removeStopWordsCheckbox.checked) {
            search.tokenizer = new JsSearch.StopWordsTokenizer(search.tokenizer);
        }

        search.indexStrategy = eval('new ' + indexStrategySelect.value + '()');
        search.sanitizer = eval('new ' + sanitizerSelect.value + '()');;

        if (tfIdfRankingCheckbox.checked) {
            search.searchIndex = new JsSearch.TfIdfSearchIndex('uid');
        } else {
            search.searchIndex = new JsSearch.UnorderedSearchIndex();
        }

        if (indexOnTvCoDauCheckbox.checked) {
            search.addIndex("tvCoDau");
        }

        if (indexOnTvKhongDauCheckbox.checked) {
            search.addIndex("tvKhongDau");
        }

        if (indexOnTiengTrungCheckbox.checked) {
            // search.addIndex("tiengTrung"); // not working atm
        }

        search.addDocuments(allWords);
    };

    const indexedWordsTable = document.getElementById('indexedWordsTable');
    const indexedWordsTBody = indexedWordsTable.tBodies[0];
    const searchInput = document.getElementById('searchInput');
    const wordCountBadge = document.getElementById('wordCountBadge');

    const updateWordsTable = function (words) {
        indexedWordsTBody.innerHTML = '';

        const tokens = search.tokenizer.tokenize(searchInput.value);

        for (let i = 0, length = words.length; i < length; i++) {
            const word = words[i];

            const uidColumn = document.createElement('td');
            uidColumn.innerText = word.uid;

            const tvCoDauColumn = document.createElement('td');
            tvCoDauColumn.innerHTML = word.tvCoDau;

            const tvKhongDauColumn = document.createElement('td');
            tvKhongDauColumn.innerText = word.tvKhongDau;

            const tiengTrungColumn = document.createElement('td');
            tiengTrungColumn.classList.add("tieng-trung");
            tiengTrungColumn.innerHTML = word.tiengTrung;

            const copyColumn = document.createElement('td');
            const copyButton = document.createElement("button");
            copyButton.innerText = "Copy";
            copyButton.className += "btn btn-info";
            copyButton.onclick = onClickCopy;
            copyColumn.append(copyButton)

            const row = document.createElement('tr');
            row.appendChild(uidColumn);
            row.appendChild(tvCoDauColumn);
            row.appendChild(tvKhongDauColumn);
            row.appendChild(tiengTrungColumn);
            row.appendChild(copyColumn);

            indexedWordsTBody.appendChild(row);
        }
    };

    const updateWordCountAndTable = function () {
        updateWordCount(results.length);

        if (results.length > 0) {
            updateWordsTable(results);
        } else if (!!searchInput.value) {
            updateWordsTable([]);
        } else {
            updateWordCount(allWords.length);
            updateWordsTable(allWords);
        }
    };

    const searchWords = function () {
        results = search.search(searchInput.value);
        updateWordCountAndTable();
    };

    searchInput.oninput = searchWords;

    const updateWordCount = function (numWords) {
        wordCountBadge.innerText = numWords + ' kết quả';
    };
    const hideElement = function (element) {
        element.className += ' d-none';
    };
    const showElement = function (element) {
        element.className = element.className.replace(/\s*d-none/, '');
    };

    const xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            const content = xmlhttp.responseText;
            const lines = content.replace("\r\n", "\n").split("\n").slice(1);
            lines.forEach((line, i) => {
                if (!line) {
                    return;
                }

                const cells = line.split(";").map(x => x.trim());
                allWords.push({
                    uid: i.toString(),
                    tvKhongDau: removeDau(cells[0]),
                    tvCoDau: cells[0],
                    tiengTrung: cells[1],
                });
            });

            updateWordCount(allWords.length);

            const loadingProgressBar = document.getElementById('loadingProgressBar');
            hideElement(loadingProgressBar);
            showElement(indexedWordsTable);

            rebuildSearchIndex();
            updateWordsTable(allWords);
        }
    }
    xmlhttp.open('GET', 'dictionary.csv', true);
    xmlhttp.send();

    /**
     * https://gist.github.com/hu2di/e80d99051529dbaa7252922baafd40e3#gistcomment-2983674
     * @param {*} str 
     * @returns 
     */
    function removeDau(str) {
        return str
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "")
            .replace(/đ/g, "d")
            .replace(/Đ/g, "D");
    }

    function onClickCopy(e) {
        const row = e.currentTarget.closest("tr");
        const chinese = row.querySelector(".tieng-trung").innerText;
        setClipboard(chinese);
    }

    function setClipboard(text) {
        var type = "text/plain";
        var blob = new Blob([text], { type });
        var data = [new ClipboardItem({ [type]: blob })];

        navigator.clipboard.write(data).then(
            function () {
                /* success */
                Toastify({
                    text: "Text's copied to clipboard. You can Ctrl+V now.",
                    backgroundColor: "linear-gradient(to right, #00b09b, #96c93d)",
                    className: "success",
                }).showToast();
            },
            function () {
                /* failure */
                console.log(1);
            }
        );
    }
})();